import actionTypes from '../constants/actionTypes';
import playersReducer, { initialState } from './playersReducer';

describe('playersReducer', () => {
	it('should set default state', () => {
		const state = playersReducer(undefined, { type: '@@INIT' });
		expect(state).toEqual(initialState);
	});

	it('should set player one avatar source string', () => {
		const action = {
			type: actionTypes.PLAYER_ONE_AVATAR,
			payload: 'Pink',
		};
		const result = { ...initialState, playerOneAvatarSrc: 'Pink' };
		const state = playersReducer(undefined, action);
		expect(state).toEqual(result);
	});

	it('should set player two avatar source string', () => {
		const action = {
			type: actionTypes.PLAYER_TWO_AVATAR,
			payload: 'Orange',
		};
		const result = { ...initialState, playerTwoAvatarSrc: 'Orange' };
		const state = playersReducer(undefined, action);
		expect(state).toEqual(result);
	});

	it('should set player one name', () => {
		const action = {
			type: actionTypes.PLAYER_ONE_NAME,
			payload: 'This is some name',
		};
		const result = { ...initialState, playerOneName: 'This is some name' };
		const state = playersReducer(undefined, action);
		expect(state).toEqual(result);
	});

	it('should set player two name', () => {
		const action = {
			type: actionTypes.PLAYER_TWO_NAME,
			payload: 'This is some other name',
		};
		const result = { ...initialState, playerTwoName: 'This is some other name' };
		const state = playersReducer(undefined, action);
		expect(state).toEqual(result);
	});

	it('should switch players turn', () => {
		const action = {
			type: actionTypes.PLAYER_TURN,
		};
		const result = { ...initialState, isPlayerOneTurn: false, isPlayerTwoTurn: true };
		const state = playersReducer(undefined, action);
		expect(state).toEqual(result);
	});

	it('should add wins to player one', () => {
		const action = {
			type: actionTypes.PLAYER_WINNER,
			payload: 'X',
		};
		const result = { ...initialState, playerOneWins: 1, playerOneIsCurrentWinner: true };
		const state = playersReducer(undefined, action);
		expect(state).toEqual(result);
	});

	it('should add wins to player two', () => {
		const action = {
			type: actionTypes.PLAYER_WINNER,
			payload: 'O',
		};
		const result = { ...initialState, playerTwoWins: 1, playerTwoIsCurrentWinner: true };
		const state = playersReducer(undefined, action);
		expect(state).toEqual(result);
	});

	it('should reset playerOneIsCurrentWinner and playerTwoIsCurrentWinner', () => {
		const action = {
			type: actionTypes.CURRENT_WINNER_RESET,
		};
		const state = playersReducer(undefined, action);
		expect(state).toEqual(initialState);
	});
});
