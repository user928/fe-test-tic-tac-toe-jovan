import actionTypes from '../constants/actionTypes';
import { addRandomWinsToUsers } from '../../helpers';

export const initialState = {
	allData: [],
	paginationVal: 'first',
	typingUserVal: '',
	loading: true,
	error: false,
	isRequestedAllFired: false,
};

export default function leaderboardReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.REQUEST_ALL_USERS:
			return {
				...state,
				loading: false,
				error: false,
				isRequestedAllFired: true,
			};
		case actionTypes.RECEIVED_ALL_USERS:
			const responseWithWinsAll = addRandomWinsToUsers(action.payload.data);
			const paginationVal = action.payload.headers.link.split('/users?since=')[1].split('&per_page=')[0];
			return {
				...state,
				allData: [...state.allData, ...responseWithWinsAll],
				paginationVal,
				loading: false,
				error: false,
			};
		case actionTypes.RECEIVED_ALL_USERS_ERROR:
			return {
				...state,
				loading: false,
				error: action.payload,
			};
		case actionTypes.REQUEST_SINGLE_USER:
			return {
				...state,
				loading: false,
				error: false,
				isRequestedAllFired: false,
			};
		case actionTypes.RECEIVED_SINGLE_USER:
			const responseWithWinsUser = addRandomWinsToUsers(action.payload.data.items);
			return {
				...state,
				allData: responseWithWinsUser,
				loading: false,
				error: false,
			};
		case actionTypes.RECEIVED_SINGLE_USER_ERROR:
			return {
				...state,
				loading: false,
				error: action.payload,
			};
		case actionTypes.TYPING_USER:
			return {
				...state,
				typingUserVal: action.payload,
				loading: false,
				error: false,
			};
		case actionTypes.RESET_USERS:
			return initialState;
		default:
			return state;
	}
}
