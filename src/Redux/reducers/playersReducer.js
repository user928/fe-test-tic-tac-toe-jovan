import actionTypes from '../constants/actionTypes';

export const initialState = {
	playerOneAvatarSrc: 'Blue',
	playerTwoAvatarSrc: 'Green',
	playerOneName: '',
	playerTwoName: '',
	isPlayerOneTurn: true,
	isPlayerTwoTurn: false,
	playerOneWins: 0,
	playerTwoWins: 0,
	playerOneIsCurrentWinner: false,
	playerTwoIsCurrentWinner: false,
};

export default function playersReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.PLAYER_ONE_AVATAR:
			return {
				...state,
				playerOneAvatarSrc: action.payload,
			};
		case actionTypes.PLAYER_TWO_AVATAR:
			return {
				...state,
				playerTwoAvatarSrc: action.payload,
			};
		case actionTypes.PLAYER_ONE_NAME:
			return {
				...state,
				playerOneName: action.payload,
			};
		case actionTypes.PLAYER_TWO_NAME:
			return {
				...state,
				playerTwoName: action.payload,
			};
		case actionTypes.PLAYER_TURN:
			return {
				...state,
				isPlayerOneTurn: !state.isPlayerOneTurn,
				isPlayerTwoTurn: !state.isPlayerTwoTurn,
			};
		case actionTypes.PLAYER_WINNER:
			if (action.payload === 'X') {
				return {
					...state,
					playerOneWins: state.playerOneWins + 1,
					playerOneIsCurrentWinner: true,
				};
			} else {
				return {
					...state,
					playerTwoWins: state.playerTwoWins + 1,
					playerTwoIsCurrentWinner: true,
				};
			}
		case actionTypes.CURRENT_WINNER_RESET:
			return {
				...state,
				playerOneIsCurrentWinner: false,
				playerTwoIsCurrentWinner: false,
			};
		case actionTypes.PLAYER_WINNS_RESET:
			return {
				...state,
				playerOneWins: 0,
				playerTwoWins: 0,
			};
		default:
			return state;
	}
}
