import { combineReducers } from 'redux';

import playersReducer from './reducers/playersReducer';
import leaderboardReducer from './reducers/leaderboardReducer';

export default combineReducers({ playersReducer, leaderboardReducer });
