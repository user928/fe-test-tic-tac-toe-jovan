import actionTypes from '../constants/actionTypes';

export const playerOneAvatarAction = payload => ({
	type: actionTypes.PLAYER_ONE_AVATAR,
	payload,
});

export const playerTwoAvatarAction = payload => ({
	type: actionTypes.PLAYER_TWO_AVATAR,
	payload,
});

export const playerOneNameAction = payload => ({
	type: actionTypes.PLAYER_ONE_NAME,
	payload,
});

export const playerTwoNameAction = payload => ({
	type: actionTypes.PLAYER_TWO_NAME,
	payload,
});

export const playerTurnAction = () => ({
	type: actionTypes.PLAYER_TURN,
});

export const playerWinnerAction = payload => ({
	type: actionTypes.PLAYER_WINNER,
	payload,
});

export const playerWinnsResetAction = () => ({
	type: actionTypes.PLAYER_WINNS_RESET,
});

export const currentWinnerResetAction = () => ({
	type: actionTypes.CURRENT_WINNER_RESET,
});
