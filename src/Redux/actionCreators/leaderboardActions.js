import api from '../../api';
import actionTypes from '../constants/actionTypes';

export const getAllUsersAction = paginationVal => {
	return function(dispatch) {
		return api
			.getAllUsers(paginationVal)
			.then(
				dispatch({
					type: actionTypes.REQUEST_ALL_USERS,
				})
			)
			.then(response => {
				dispatch({
					type: actionTypes.RECEIVED_ALL_USERS,
					payload: response,
				});
			})
			.catch(response => {
				dispatch({
					type: actionTypes.RECEIVED_ALL_USERS_ERROR,
					payload: response,
				});
			});
	};
};

export const getSingleUserAction = username => {
	return function(dispatch) {
		return api
			.getSingleUser(username)
			.then(
				dispatch({
					type: actionTypes.REQUEST_SINGLE_USER,
				})
			)
			.then(response => {
				dispatch({
					type: actionTypes.RECEIVED_SINGLE_USER,
					payload: response,
				});
			})
			.catch(response => {
				dispatch({
					type: actionTypes.RECEIVED_SINGLE_USER_ERROR,
					payload: response,
				});
			});
	};
};

export const resetUsersAction = () => ({
	type: actionTypes.RESET_USERS,
});

export const typingUserAction = payload => ({
	type: actionTypes.TYPING_USER,
	payload,
});
