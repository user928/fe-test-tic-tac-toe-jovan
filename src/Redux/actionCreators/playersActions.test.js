import actionTypes from '../constants/actionTypes';
import * as actions from './playersActions';

describe('playersActions', () => {
	it('should send player one avatar', () => {
		const action = actions.playerOneAvatarAction('Orange');
		expect(action).toEqual({
			type: actionTypes.PLAYER_ONE_AVATAR,
			payload: 'Orange',
		});
	});

	it('should send player two avatar', () => {
		const action = actions.playerTwoAvatarAction('Pink');
		expect(action).toEqual({
			type: actionTypes.PLAYER_TWO_AVATAR,
			payload: 'Pink',
		});
	});

	it('should send player one name', () => {
		const action = actions.playerOneNameAction('this is some name name');
		expect(action).toEqual({
			type: actionTypes.PLAYER_ONE_NAME,
			payload: 'this is some name name',
		});
	});

	it('should send player two name', () => {
		const action = actions.playerTwoNameAction('this is some other name');
		expect(action).toEqual({
			type: actionTypes.PLAYER_TWO_NAME,
			payload: 'this is some other name',
		});
	});

	it('should send winner symbol', () => {
		const action = actions.playerWinnerAction('X');
		expect(action).toEqual({
			type: actionTypes.PLAYER_WINNER,
			payload: 'X',
		});
	});
});
