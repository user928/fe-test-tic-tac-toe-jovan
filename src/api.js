import axios from 'axios';

//if token expire generate new token at https://github.com/settings/tokens
const token = '';

const config = {
	headers: {
		'Content-Type': 'application/json',
		Accept: 'application/json',
		Authorization: `token ${token}`,
	},
};

const getUserMe = userId => axios.get(`https://api.github.com/user`, config); //check if token has expired

const getAllUsers = userId => axios.get(`https://api.github.com/users?since=${userId}&per_page=5`);

const getSingleUser = username =>
	axios.get(`https://api.github.com/search/users?q=${username}&page,per_page,sort,order`);

export default {
	getUserMe,
	getAllUsers,
	getSingleUser,
};
