export const findGameWinner = symbolsArray => {
	const indexCombinations = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]];
	for (let i = 0; i < indexCombinations.length; i++) {
		const [indexA, indexB, indexC] = indexCombinations[i];

		if (
			symbolsArray[indexA] &&
			symbolsArray[indexA] === symbolsArray[indexB] &&
			symbolsArray[indexA] === symbolsArray[indexC]
		) {
			return { winSymbol: symbolsArray[indexA], winIndex: [indexA, indexB, indexC] };
		}
	}
	return null;
};

export const addRandomWinsToUsers = data => {
	const dataCopy = [...data];
	dataCopy.forEach(item => {
		item.wins = Math.floor(Math.random() * 100);
	});
	return dataCopy;
};
