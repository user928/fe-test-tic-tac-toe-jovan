import { findGameWinner } from './helpers';

const mockData = {
	indexCombo0: ['X', 'X', 'X', null, 'O', 'O', null, null, null],  // [0, 1, 2],
	indexCombo1: ['X', null, 'X', 'O', 'O', 'O', null, 'X', null], 	 // [3, 4, 5]
	indexCombo2: ['X', 'O', 'X', 'O', 'X', 'O', 'X', 'X', 'X'],		 // [6, 7, 8]
	indexCombo3: ['X', null, null, 'X', 'O', 'O', 'X', null, null], // [0, 3, 6],
	indexCombo4: [null, 'O', null, 'X', 'O', null, 'X', 'O', null], // [1, 4, 7],
	indexCombo5: ['O', null, 'X', 'O', null, 'X', null, null, 'X'], // [2, 5, 8],
	indexCombo6: ['O', 'X', null, null, 'O', 'X', null, null, 'O'], // [0, 4, 8],
	indexCombo7: [null, 'O', 'X', 'O', 'X', null, 'X', null, null], // [[2, 4, 6],
	indexCombo8: ['O', 'X', 'O', 'O', 'X', 'X', 'X', 'O', 'O'], // no winner / draw,
};

describe('findGameWinner test', function() {
	it('returns true for X || O for correct index combination (win)', function() {
		expect(findGameWinner(mockData.indexCombo0)).toEqual({ winIndex: [0, 1, 2], winSymbol: 'X' });
		expect(findGameWinner(mockData.indexCombo1)).toEqual({ winIndex: [3, 4, 5], winSymbol: 'O' });
		expect(findGameWinner(mockData.indexCombo2)).toEqual({ winIndex: [6, 7, 8], winSymbol: 'X' });
		expect(findGameWinner(mockData.indexCombo3)).toEqual({ winIndex: [0, 3, 6], winSymbol: 'X' });
		expect(findGameWinner(mockData.indexCombo4)).toEqual({ winIndex: [1, 4, 7], winSymbol: 'O' });
		expect(findGameWinner(mockData.indexCombo5)).toEqual({ winIndex: [2, 5, 8], winSymbol: 'X' });
		expect(findGameWinner(mockData.indexCombo6)).toEqual({ winIndex: [0, 4, 8], winSymbol: 'O' });
		expect(findGameWinner(mockData.indexCombo7)).toEqual({ winIndex: [2, 4, 6], winSymbol: 'X' });
	});

	it('returns null for non correct order', function() {
		expect(findGameWinner(mockData.indexCombo8)).toEqual(null);
	});
});
