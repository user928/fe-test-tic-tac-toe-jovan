import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from './Redux/store';
import Help from './Components/Help/Help';
import Leaderboard from './Components/Leaderboard/Leaderboard';
import Home from './Components/Home/Home';
import SinglePlayer from './Components/Player/SinglePlayer/SinglePlayer';
import MultiPlayer from './Components/Player/MultiPlayer/MultiPlayer';
import Game from './Components/Game/Game/Game';
import styles from './App.module.scss';

const App = () => (
	<div className={styles.MainBg}>
		<Switch>
			<Route path="/help" component={Help} />
			<Route path="/singlePlayer" component={SinglePlayer} />
			<Route path="/multiPlayer" component={MultiPlayer} />
			<Route path="/game" component={Game} />
			<Route path="/leaderboard" component={Leaderboard} />
			<Route exact path="/" component={Home} />
			<Route component={Home} />
		</Switch>
	</div>
);

const store = configureStore();
export default class extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<App {...this.props} />
			</Provider>
		);
	}
}
