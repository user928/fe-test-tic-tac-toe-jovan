import React from 'react';
import { connect } from 'react-redux';
import AvatarInGame from '../../Player/AvatarInGame/AvatarInGame';
import HomeButton from '../../Buttons/HomeButton/HomeButton';
import Board from '../Board/Board';
import styles from './Game.module.scss';

const _Game = props => {
	const {
		playerOneAvatarSrc,
		playerTwoAvatarSrc,
		playerOneName,
		playerTwoName,
		isPlayerOneTurn,
		isPlayerTwoTurn,
		playerOneWins,
		playerTwoWins,
		playerOneIsCurrentWinner,
		playerTwoIsCurrentWinner,
	} = props.playersReducer;

	return (
		<div className={styles.FlexCol}>
			<HomeButton className={styles.HomeBtn} />

			<div className={styles.WrapperMain}>
				<AvatarInGame
					playerName={playerOneName}
					avatarSrc={playerOneAvatarSrc}
					isPlayerTurn={isPlayerOneTurn}
					isPlayerOneTurn={isPlayerOneTurn}
					playerWins={playerOneWins}
					isCurrentWinner={playerOneIsCurrentWinner}
				/>

				<AvatarInGame
					playerName={playerTwoName}
					avatarSrc={playerTwoAvatarSrc}
					isPlayerTurn={isPlayerTwoTurn}
					isPlayerTwoTurn={isPlayerTwoTurn}
					playerWins={playerTwoWins}
					isCurrentWinner={playerTwoIsCurrentWinner}
				/>
			</div>

			<Board playerOneName={props.playerOneName} playerTwoName={props.playerTwoName} />
		</div>
	);
};

export const mapStateToProps = store => ({ playersReducer: store.playersReducer });

export default connect(mapStateToProps)(_Game);
