import React from 'react';
import { connect } from 'react-redux';
import {
	playerTurnAction,
	playerWinnerAction,
	currentWinnerResetAction,
} from '../../../Redux/actionCreators/playersActions';
import Symbol from '../Symbol/Symbol';
import RestartButton from '../../Buttons/RestartButton/RestartButton';
import { findGameWinner } from '../../../helpers';
import styles from './Board.module.scss';

const initialState = {
	symbolsArr: Array(9).fill(null),
};

class Board extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			...initialState,
		};
		this.isGameActive = true;
	}

	componentDidMount() {
		this.props.currentWinnerResetAction();
	}

	restartGame = () => {
		this.setState({
			...initialState,
		});
		this.isGameActive = true;
		this.props.currentWinnerResetAction();
	};

	handleClick = i => {
		//copy array so we can change it
		const modifiedSymbolsArr = [...this.state.symbolsArr];

		//disable click if symbol for current index || winner exist
		if (modifiedSymbolsArr[i] || findGameWinner(modifiedSymbolsArr)) {
			return;
		}
		//set symbol instead of null (in clicked place / index)
		modifiedSymbolsArr[i] = this.props.isPlayerOneTurn ? 'X' : 'O';
		this.props.playerTurnAction();
		this.setState({
			symbolsArr: modifiedSymbolsArr,
		});
	};

	renderSymbol = (i, winIndex) => {
		let isWinnerSymbol = false;

		if (winIndex && winIndex.indexOf(i) > -1) {
			isWinnerSymbol = true;
		}
		return (
			<Symbol
				symbol={this.state.symbolsArr[i]}
				onClick={() => this.handleClick(i)}
				isWinnerSymbol={isWinnerSymbol}
				isPlayerOneTurn={this.props.isPlayerOneTurn}
				key={i}
			/>
		);
	};

	render() {
		const winner = findGameWinner(this.state.symbolsArr);
		let gameEndInfoTxt = 'Let the battle begin!';
		let GameEndInfoStyles = styles.GameEndInfoStylesNormal;

		if (winner && winner.winSymbol) {
			//todo find better way to fire this only once
			this.isGameActive && this.props.playerWinnerAction(winner.winSymbol);

			if (winner.winSymbol === 'X') {
				gameEndInfoTxt = `Winner is: ${this.props.playerOneName}`;
			} else {
				gameEndInfoTxt = `Winner is: ${this.props.playerTwoName}`;
			}
			GameEndInfoStyles = styles.GameEndInfoStylesWinner;
			this.isGameActive = false;
		} else if (this.state.symbolsArr.indexOf(null) < 0) {
			gameEndInfoTxt = 'Game is draw !';
			this.isGameActive = false;
		}

		return (
			<div className={styles.FlexCol}>
				<h2 className={GameEndInfoStyles}>{gameEndInfoTxt}</h2>
				<div className={styles.WrapperGrid}>
					{this.state.symbolsArr.map((item, i) => {
						return this.renderSymbol(i, winner && winner.winIndex);
					})}
				</div>
				<RestartButton onClick={this.restartGame} isGameActive={this.isGameActive} />
			</div>
		);
	}
}

export const mapStateToProps = store => ({
	isPlayerOneTurn: store.playersReducer.isPlayerOneTurn,
	playerOneName: store.playersReducer.playerOneName,
	playerTwoName: store.playersReducer.playerTwoName,
});

export const mapDispatchToProps = dispatch => ({
	playerTurnAction: () => dispatch(playerTurnAction()),
	playerWinnerAction: params => dispatch(playerWinnerAction(params)),
	currentWinnerResetAction: () => dispatch(currentWinnerResetAction()),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Board);
