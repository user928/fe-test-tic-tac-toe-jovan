import Symbol from './Symbol.js';

describe('Symbol component', () => {
	const props = {
		symbol: 'X',
		isWinnerSymbol: true,
		isPlayerOneTurn: false,
	};

	it('receive correct props', () => {
		const wrapper = mount(<Symbol {...props} />);
		expect(wrapper.props().symbol).toBe('X');
		expect(wrapper.props().isWinnerSymbol).toBe(true);
		expect(wrapper.props().isPlayerOneTurn).toBe(false);
	});

	it('render correct symbol based on prop (X)', () => {
		const wrapper = mount(<Symbol {...props} />);
		expect(wrapper.find('button').text()).toBe('X');
	});

	it('render correct css class based on prop (isWinnerSymbol: true)', () => {
		const wrapper = mount(<Symbol {...props} />);
		expect(wrapper.find('button').hasClass('WinnerStyle')).toBe(true);
	});

	it('render correct css class based on prop (isPlayerOneTurn: true)', () => {
		const props = {
			isWinnerSymbol: false,
			isPlayerOneTurn: true,
		};
		const wrapper = mount(<Symbol {...props} />);
		expect(wrapper.find('button').hasClass('PlayerOneCursor')).toBe(true);
	});

	it('render correct css class based on prop (isPlayerOneTurn: false)', () => {
		const props = {
			isWinnerSymbol: false,
			isPlayerOneTurn: false,
		};
		const wrapper = mount(<Symbol {...props} />);
		expect(wrapper.find('button').hasClass('PlayerTwoCursor')).toBe(true);
	});

	it('click on button once', () => {
		const spy = sinon.spy();
		const wrapper = mount(<Symbol onClick={spy} />);

		wrapper.find('button').simulate('click');
		expect(spy.calledOnce).toBe(true);
	});
});
