import React from 'react';
import styles from './Symbol.module.scss';

function Symbol(props) {
	let className;
	if (props.isWinnerSymbol) {
		className = styles.WinnerStyle;
	} else if (props.isPlayerOneTurn) {
		className = styles.PlayerOneCursor;
	} else {
		className = styles.PlayerTwoCursor;
	}

	return (
		<button className={className} onClick={props.onClick}>
			{props.symbol}
		</button>
	);
}

export default Symbol;
