import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { playerOneAvatarAction, playerOneNameAction } from '../../../Redux/actionCreators/playersActions';
import PlayerCreation from '../PlayerCreation/PlayerCreation';
import HomeButton from '../../Buttons/HomeButton/HomeButton';
import styles from './SinglePlayer.module.scss';

const SinglePlayer = props => (
	<>
		<HomeButton />
		<div className={styles.FlexCol}>
			<PlayerCreation
				avatarAction={props.playerOneAvatarAction}
				avatarSrc={props.playerOneAvatarSrc}
				playerNameAction={props.playerOneNameAction}
				playerName={props.playerOneName}
			/>
			<p>Single player mode is coming in next version ;)</p>
			<div>
				Try
				<Link className={styles.Button} to="/multiPlayer">
					MultiPlayer
				</Link>
				instead
			</div>
		</div>
	</>
);

export const mapStateToProps = store => ({
	playerOneAvatarSrc: store.playersReducer.playerOneAvatarSrc,
});

export const mapDispatchToProps = dispatch => ({
	playerOneAvatarAction: params => dispatch(playerOneAvatarAction(params)),
	playerOneNameAction: params => dispatch(playerOneNameAction(params)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SinglePlayer);
