import React from 'react';
import Avatar from '../Avatar/Avatar';
import styles from './PlayerCreation.module.scss';

const PlayerCreation = props => {
	const handleInputChange = val => {
		props.playerNameAction(val.target.value);
		props.playerWinnsResetAction(); //reset wins if name change
	};

	return (
		<div className={styles.Wrapper}>
			<div className={styles.WrapperIcon}>
				<Avatar avatarSrc={props.avatarSrc} />

				<div className={styles.FlexRow}>
					<button
						onClick={() => props.avatarAction('Blue')}
						className={props.avatarSrc === 'Blue' ? styles.AvatarPickerBlueActive : styles.AvatarPickerBlue}
					/>

					<button
						onClick={() => props.avatarAction('Orange')}
						className={
							props.avatarSrc === 'Orange' ? styles.AvatarPickerOrangeActive : styles.AvatarPickerOrange
						}
					/>

					<button
						onClick={() => props.avatarAction('Green')}
						className={
							props.avatarSrc === 'Green' ? styles.AvatarPickerGreenActive : styles.AvatarPickerGreen
						}
					/>

					<button
						onClick={() => props.avatarAction('Pink')}
						className={props.avatarSrc === 'Pink' ? styles.AvatarPickerPinkActive : styles.AvatarPickerPink}
					/>
				</div>
			</div>

			<input
				className={styles.Input}
				type="text"
				onChange={handleInputChange}
				value={props.playerName}
				placeholder="Enter your name..."
				required
			/>
		</div>
	);
};

export default PlayerCreation;
