import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
	playerOneAvatarAction,
	playerTwoAvatarAction,
	playerOneNameAction,
	playerTwoNameAction,
	playerWinnsResetAction,
} from '../../../Redux/actionCreators/playersActions';
import PlayerCreation from '../PlayerCreation/PlayerCreation';
import HomeButton from '../../Buttons/HomeButton/HomeButton';
import styles from './MultiPlayer.module.scss';

const MultiPlayer = props => {
	const { playerOneName, playerTwoName, playerOneAvatarSrc, playerTwoAvatarSrc } = props.playersReducer;

	let linkClassName;
	if (playerOneName && playerTwoName) {
		linkClassName = styles.Enabled;
	} else {
		linkClassName = styles.Disabled;
	}

	return (
		<>
			<HomeButton />

			<div className={styles.FlexCol}>
				<div className={styles.FlexRow}>
					<div className={styles.FlexCol}>
						<p>Player One</p>
						<PlayerCreation
							playerName={playerOneName}
							avatarSrc={playerOneAvatarSrc}
							playerNameAction={props.playerOneNameAction}
							avatarAction={props.playerOneAvatarAction}
							playerWinnsResetAction={props.playerWinnsResetAction}
						/>
					</div>

					<div className={styles.FlexCol}>
						<p>Player Two</p>
						<PlayerCreation
							playerName={playerTwoName}
							avatarSrc={playerTwoAvatarSrc}
							playerNameAction={props.playerTwoNameAction}
							avatarAction={props.playerTwoAvatarAction}
							playerWinnsResetAction={props.playerWinnsResetAction}
						/>
					</div>
				</div>

				<Link to="/game" className={linkClassName}>
					START
				</Link>
			</div>
		</>
	);
};

export const mapStateToProps = store => ({ playersReducer: store.playersReducer });

export const mapDispatchToProps = dispatch => ({
	playerOneAvatarAction: params => dispatch(playerOneAvatarAction(params)),
	playerTwoAvatarAction: params => dispatch(playerTwoAvatarAction(params)),
	playerOneNameAction: params => dispatch(playerOneNameAction(params)),
	playerTwoNameAction: params => dispatch(playerTwoNameAction(params)),
	playerWinnsResetAction: () => dispatch(playerWinnsResetAction()),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(MultiPlayer);
