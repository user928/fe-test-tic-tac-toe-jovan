import React from 'react';
import Avatar from '../Avatar/Avatar';
import SVGIcon from '../../Utils/SVGIcon/SVGIcon';
import styles from './AvatarInGame.module.scss';

const AvatarInGame = props => {
	let playerClassNames;
	if (props.isPlayerTurn) {
		playerClassNames = styles.PlayerPlaying;
	} else if (props.isCurrentWinner) {
		playerClassNames = styles.PlayerWinner;
	} else {
		playerClassNames = styles.Player;
	}

	return (
		<div className={playerClassNames}>
			<div className={styles.AvatarWrapper}>
				<Avatar avatarSrc={props.avatarSrc} />
				{props.isPlayerOneTurn && <SVGIcon icon="BubbleX" size={30} wrapperClassName={styles.AvatarBubbleX} />}
				{props.isPlayerTwoTurn && <SVGIcon icon="BubbleO" size={30} wrapperClassName={styles.AvatarBubbleO} />}
			</div>

			<p className={styles.PlayerName}>{props.playerName}</p>
			<p className={styles.PlayerWinNumb}>Wins: {props.playerWins}</p>
		</div>
	);
};

export default AvatarInGame;
