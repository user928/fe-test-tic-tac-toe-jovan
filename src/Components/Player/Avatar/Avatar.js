import React from 'react';
import styles from './Avatar.module.scss';

const Avatar = props => {
	let className;

	if (props.avatarSrc === 'Blue') {
		className = styles.AvatarImageBlue;
	} else if (props.avatarSrc === 'Green') {
		className = styles.AvatarImageGreen;
	} else if (props.avatarSrc === 'Orange') {
		className = styles.AvatarImageOrange;
	} else {
		className = styles.AvatarImagePink;
	}

	return <div className={className} />;
};

export default Avatar;
