import React from 'react';
import HomeButton from '../Buttons/HomeButton/HomeButton';

const Help = () => (
	<>
		<HomeButton />
		<h1>Help Page</h1>
		<p>
			Tic-tac-toe (also known as noughts and crosses or Xs and Os) is a paper-and-pencil game for two players, X
			and O, who take turns marking the spaces in a 3×3 grid.
		</p>
		<p>
			The player who succeeds in placing three of their marks in a horizontal, vertical, or diagonal row wins the
			game
		</p>
		<p>
			You can find more info at <a href="https://en.wikipedia.org/wiki/Tic-tac-toe">wikipedia</a>
		</p>

		<div style={{ width: '100%', height: 0, paddingBottom: '75%', position: 'relative' }}>
			<iframe
				title="tic tac toe gif"
				src="https://giphy.com/embed/xTk9ZG1UH3tI5YKoXC"
				width="100%"
				height="100%"
				style={{ position: 'absolute' }}
				frameBorder="0"
				className="giphy-embed"
				allowFullScreen
			>
				{' '}
			</iframe>
		</div>
	</>
);

export default Help;
