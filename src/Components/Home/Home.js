import React from 'react';
import { Link } from 'react-router-dom';
import HelpButton from '../Buttons/HelpButton/HelpButton';
import styles from './Home.module.scss';

const Home = () => (
	<div className={styles.FlexCol}>
		<HelpButton />
		<h1>Tic Tac Toe</h1>

		<span className={styles.MarginBottom}>Choose game mode</span>
		<div className={styles.GameMode}>
			<Link className={styles.GameModeInner} to="/singlePlayer">
				<div className={styles.PlayerOne} />
				<span className={styles.GameModeTxt}>Single-Player</span>
			</Link>
			Or
			<Link className={styles.GameModeInner} to="/multiPlayer">
				<div className={styles.FlexRow}>
					<div className={styles.PlayerOneMulti} />
					<div className={styles.PlayerTwoMulti} />
				</div>
				<span className={styles.GameModeTxt}>Multiplayer</span>
			</Link>
		</div>

		<Link to="/leaderboard" className={styles.BestPlayersBtn}>
			Best Player Rankings
		</Link>
	</div>
);

export default Home;
