import React from 'react';
import SVGIcon from '../SVGIcon/SVGIcon';
import styles from './Loading.module.scss';

const Loading = props => {
	let iconSize;
	let txtSize;
	if (props.size === 'small') {
		iconSize = 15;
		txtSize = styles.LoadingTxtSmall;
	} else {
		iconSize = 20;
		txtSize = styles.LoadingTxtLarge;
	}

	return (
		<div className={styles.FlexRow}>
			<SVGIcon wrapperClassName={styles.LoadingIco} icon="Loading" size={iconSize} />
			<span className={txtSize}>Loading</span>
		</div>
	);
};

export default Loading;
