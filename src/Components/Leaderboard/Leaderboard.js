import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
	getAllUsersAction,
	getSingleUserAction,
	resetUsersAction,
	typingUserAction,
} from '../../Redux/actionCreators/leaderboardActions';
import HomeButton from '../Buttons/HomeButton/HomeButton';
import Loading from '../Utils/Loading/Loading';
import SVGIcon from '../Utils/SVGIcon/SVGIcon';
import styles from './Leaderboard.module.scss';

class Leaderboard extends Component {
	componentDidMount() {
		this.callAPI();
	}

	callAPI = () => {
		if (this.props.leaderboardReducer.typingUserVal.length !== 0) {
			this.props.getSingleUserAction(this.props.leaderboardReducer.typingUserVal);
		} else {
			this.props.getAllUsersAction(this.props.leaderboardReducer.paginationVal);
		}
	};

	resetCallAPI = () => {
		this.props.resetUsersAction();
		this.props.getAllUsersAction(this.props.leaderboardReducer.paginationVal);
	};

	handleInputChange = inputValue => {
		this.props.typingUserAction(inputValue.currentTarget.value);
	};

	handleKeyPress = e => {
		if (e.key === 'Enter') {
			this.callAPI();
		}
	};

	render() {
		const { allData, typingUserVal, isRequestedAllFired, loading, error } = this.props.leaderboardReducer;

		return (
			<>
				<HomeButton />

				<div className={styles.PlayersSearch}>
					<div className={styles.PlayersSearchInputWrapper}>
						<input
							className={styles.PlayersSearchInput}
							type="text"
							onChange={this.handleInputChange}
							value={typingUserVal}
							onKeyPress={this.handleKeyPress}
						/>
						{typingUserVal.length !== 0 && (
							<button className={styles.ButtonClearTxt} onClick={this.resetCallAPI}>
								<SVGIcon icon="ClearText" size={20} />
							</button>
						)}
					</div>

					<button className={styles.Button} onClick={this.callAPI}>
						{loading ? <Loading size="small" /> : 'Search for players'}
					</button>
				</div>

				{error ? (
					<h1>Error: {error.message} :(</h1>
				) : !!allData && allData.length > 0 ? (
					<div className={styles.FlexCol}>
						<ul className={styles.PlayersList}>
							{allData.map(item => (
								<li className={styles.Player} key={item.id}>
									<p className={styles.FlexCol}>
										<img className={styles.PlayerImage} src={item.avatar_url} alt="player" />
									</p>
									<p className={styles.FlexCol}>
										<span className={styles.PlayerTxt}>Player: </span>
										<span className={styles.PlayerTxtBold}>{item.login}</span>
									</p>
									<p className={styles.FlexCol}>
										<span className={styles.PlayerTxt}>Wins: </span>
										<span className={styles.PlayerTxtBold}>{item.wins}</span>
									</p>
								</li>
							))}
						</ul>

						{isRequestedAllFired && (
							<button className={styles.Button} onClick={this.callAPI}>
								{loading ? <Loading size="small" /> : 'Load more'}
							</button>
						)}
					</div>
				) : !loading ? (
					<h1>Not found</h1>
				) : null}
			</>
		);
	}
}

export const mapStateToProps = store => ({
	leaderboardReducer: store.leaderboardReducer,
});

export const mapDispatchToProps = dispatch => ({
	getAllUsersAction: params => dispatch(getAllUsersAction(params)),
	getSingleUserAction: params => dispatch(getSingleUserAction(params)),
	resetUsersAction: () => dispatch(resetUsersAction()),
	typingUserAction: params => dispatch(typingUserAction(params)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Leaderboard);
