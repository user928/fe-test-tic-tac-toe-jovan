import React from 'react';
import { Link } from 'react-router-dom';
import SVGIcon from '../../Utils/SVGIcon/SVGIcon';
import styles from './HelpButton.module.scss';

const HelpButton = () => (
	<Link className={styles.Wrapper} to="/help">
		<SVGIcon icon="Help" size={40} wrapperClassName={styles.Icon} />
		<span className={styles.How}>How to play</span>
	</Link>
);

export default HelpButton;
