import React from 'react';
import SVGIcon from '../../Utils/SVGIcon/SVGIcon';
import styles from './RestartButton.module.scss';

const RestartButton = props => {

	let classNames;
	if (props.isGameActive) {
		classNames = styles.ButtonNormal;
	} else {
		classNames = styles.ButtonRestart;
	}

	return (
		<button className={classNames} onClick={props.onClick}>
			<SVGIcon icon="Replay" size={20} wrapperClassName={styles.Icon} />
			<span className={styles.Text}>Restart Game</span>
		</button>
	);
};

export default RestartButton;
