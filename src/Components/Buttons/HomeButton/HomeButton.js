import React from 'react';
import { Link } from 'react-router-dom';
import SVGIcon from '../../Utils/SVGIcon/SVGIcon';
import styles from './HomeButton.module.scss';

const HomeButton = props => (
	<Link className={props.className ? props.className : styles.Style} to="/">
		<SVGIcon wrapperClassName={styles.Icon} icon="Home" size={20} />
	</Link>
);

export default HomeButton;
