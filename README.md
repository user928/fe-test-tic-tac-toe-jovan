<div align="center">
<h2 >Tic Tac Toe App</h1>

<h4>This is simple tic tac toe game, users can play against each other.</h4>
<h4>The first user to connect 3 symbols in a row or diagonal wins.</h4>
<img align="center" src="https://api.monosnap.com/rpc/file/download?id=e6CxrxxNTyBIeHvvFlQ0aVRtTYPtFc" alt="ingame example">

<br>

<h4>      
Demo: <a href="https://tic-tac-jovan.herokuapp.com/">Play it online</a>
</h4>

</div>

<br>
This app uses:

| Field     | Library             |
| --------- | ------------------- |
| React     | React               |
| Redux     | Redux
| CSS       | SASS, CSS Modules   |
| Testing   | Enzyme, Sinon, Jest |
| Formating | Prettier            |
</div>


<br>
If you are having problems running "yarn/npm test" on macOS High Sierra, try to install watchman

```js
$ brew install watchman
```

<br>
To start run npm install and then npm start

```js
$ npm install
$ npm start
```

<br>
Note: This app is forked from Create React App.